package com.stocksphere.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SSAppServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	private int hitCount;
	
	@Override
	public void init() throws ServletException {
		super.init();
		
		hitCount = 0;
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
		
		System.out.println("count : "+ (++hitCount));
	}
	

}
