package com.stocksphere.services;

public class SSAppServiceUtils {
	
	/**
	 * This method sets the param value null if it is undefined (from js call)
	 * @param param
	 */
	public static String setUndefinedNull(String param)
	{
		if(param != null && param.equalsIgnoreCase("undefined"))
		{
			return null;
		}
		else
		{
			return param;
		}
	}

}
