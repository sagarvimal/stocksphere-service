package com.stocksphere.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.stocksphere.common.AppConstants;
import com.stocksphere.common.AppUtils;
import com.stocksphere.common.SSComparator;
import com.stocksphere.morphia.MorphiaUtils;
import com.stocksphere.morphia.historydatavo.CompanyDetailsVO;
import com.stocksphere.morphia.historydatavo.StockDailyDataVO;
import com.stocksphere.morphia.historydatavo.StockVO;
import com.stocksphere.stockhistory.analysis.HistoryAnalyser;
import com.stocksphere.stockhistory.analysis.ScriptAllDetailsVO;
import com.stocksphere.stockhistory.analysis.ScriptHistoryAnalysisVO;

public class SSAppServiceHelper {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(SSAppServiceHelper.class);
	
	public List<ScriptAllDetailsVO> getAllDailyDetailsForScript(String scriptSymbol)
	{
		log.info("inside getAllDailyDetailsForScript for script >> "+scriptSymbol);
		List<ScriptAllDetailsVO> scriptAllDetailsVOList = null;
		
		StockVO stockVO = new MorphiaUtils().getAllDocumentsForScript(scriptSymbol);
		List<StockDailyDataVO> stockDailyDataVOList = null;
		ScriptAllDetailsVO scriptAllDetailsVO = null;
		
		if(stockVO != null)
		{
			stockDailyDataVOList = stockVO.getDailyDataVOList();
			
			if(stockDailyDataVOList != null && !stockDailyDataVOList.isEmpty())
			{
				scriptAllDetailsVOList = new ArrayList<ScriptAllDetailsVO>();
				
				for(StockDailyDataVO dailyDataVO : stockDailyDataVOList)
				{
					if(dailyDataVO.getClosePrice() != 0 && dailyDataVO.getDate() != null 
							&& dailyDataVO.getHighPrice() != 0 && dailyDataVO.getLowPrice() != 0 
							&& dailyDataVO.getOpenPrice() != 0 && dailyDataVO.getTotalTradedQuantity() != 0)
					{
						scriptAllDetailsVO = new ScriptAllDetailsVO();
						scriptAllDetailsVO.setSymbol(stockVO.getSymbol());
						scriptAllDetailsVO.setName(stockVO.getName());
						scriptAllDetailsVO.setClose(dailyDataVO.getClosePrice());
						scriptAllDetailsVO.setDate(dailyDataVO.getDate());
						scriptAllDetailsVO.setHigh(dailyDataVO.getHighPrice());
						scriptAllDetailsVO.setLow(dailyDataVO.getLowPrice());
						scriptAllDetailsVO.setOpen(dailyDataVO.getOpenPrice());
						scriptAllDetailsVO.setVolume(dailyDataVO.getTotalTradedQuantity());
						
						scriptAllDetailsVOList.add(scriptAllDetailsVO);
					}
					else
					{
						log.info("Skipping !! Some detail are not present for script >> "+stockVO.getSymbol());
					}
				}
				
				//sort data by volume
				SSComparator.COMPARISION_PARAMETER = AppConstants.COMPARISION_PARAMETER_VOLUME;
				Collections.sort(scriptAllDetailsVOList,new SSComparator());
			}
		}
		else
		{
			log.info("stockVO is null. No data present for the script >> "+scriptSymbol);
		}
		
		log.info("Exiting getAllDailyDetailsForScript for script >> "+scriptSymbol);
		
		return scriptAllDetailsVOList;
	}
	
	public List<ScriptHistoryAnalysisVO> getAllScriptNames()
	{
		log.info("Inside getAllScriptNames method");
		
		List<ScriptHistoryAnalysisVO> nameList = null;
		ScriptHistoryAnalysisVO nameVO = null;
		
		try
		{
			List<StockVO> stockVOLits = new MorphiaUtils().getAllStocks();
			
			if(stockVOLits != null && !stockVOLits.isEmpty())
			{
				nameList = new ArrayList<ScriptHistoryAnalysisVO>();
				
				for(StockVO stockVO : stockVOLits)
				{
					nameVO = new ScriptHistoryAnalysisVO();
					nameVO.setSymbol(stockVO.getSymbol());
					nameVO.setName(stockVO.getName());
					nameList.add(nameVO);
				}
			}
		}
		catch(Exception e)
		{
			log.info("Exception in getAllScriptNames");
			e.printStackTrace();
		}
		
		log.info("returning from getAllScriptNames");
		
		if(nameList != null && !nameList.isEmpty())
		{
			log.info("Size of nameList is: " + nameList.size());
		}
		else
		{
			log.info("nameList is null or empty");
		}
		
		return nameList;
	}
	
	public List<ScriptHistoryAnalysisVO> getStockList(String numberOfDaysStr,String dateFromStr, String DateToStr, String minRangeStr, String maxRangeStr)
	{
		log.info("Inside getStockList method");
		
		List<ScriptHistoryAnalysisVO> analysisVOList = null;

		try
		{
			double minRange = 0;
			double maxRange = 0;
			
			if(minRangeStr != null && maxRangeStr != null)
			{
				minRange = Double.parseDouble(minRangeStr);
				maxRange = Double.parseDouble(maxRangeStr);
			}

			Date dateTo = null;
			Date dateFrom = null;

			if(DateToStr != null && dateFromStr != null)
			{
				dateFrom = new SimpleDateFormat(AppConstants.SERVICE_DATE_FORMAT).parse(dateFromStr);
				dateTo = new SimpleDateFormat(AppConstants.SERVICE_DATE_FORMAT).parse(DateToStr);
				
				// get business dates of selected dates
				dateFrom = AppUtils.getBusinessDate(dateFrom, AppConstants.DATE_TYPE_FROM);
				dateTo = AppUtils.getBusinessDate(dateTo, AppConstants.DATE_TYPE_TO);
			}
			else
			{
				int numberOfDays = Integer.parseInt(numberOfDaysStr);
				dateFrom = AppUtils.getDateBeforeDays(numberOfDays);
				dateTo = AppUtils.getLastBusinessDate();
			}

			// Get list of all stocks
			//List<StockVO> stockVOList = new MorphiaUtils().getAllStocks(minRange, maxRange, dateFrom, dateTo);
			
			// Get list of all stocks
			List<StockVO> stockVOList = new MorphiaUtils().getAllStocks();

			HistoryAnalyser historyAnalyser = new HistoryAnalyser();
			
			//Filter stocks for price range
			if(minRange != 0 && maxRange != 0)
			{
				stockVOList = historyAnalyser.filterStocks(stockVOList, minRange, maxRange);
			}
			
			analysisVOList = historyAnalyser.analyse(stockVOList,dateFrom,dateTo);

			if(analysisVOList != null && !analysisVOList.isEmpty())
			{
				log.info("Data between the dates: " + AppUtils.getPresentableDate(dateFrom) 
						+" - "+ AppUtils.getPresentableDate(dateTo));
			}
			else
			{
				log.info("Unable to fetch data between the dates: " + AppUtils.getPresentableDate(dateFrom) 
						+" - "+ AppUtils.getPresentableDate(dateTo));
			}
		}
		catch(Exception e)
		{
			log.info("Exception in getAnalysisVOList");
			e.printStackTrace();
		}

		log.info("Returning analysisVOList");
		return analysisVOList;
	}
	
	public ScriptAllDetailsVO getAllDetailsOfStock(String symbol)
	{
		log.info("Inside getAllStockDetails method");
		ScriptAllDetailsVO scriptAllDetailsVO = null;
		try
		{
			MorphiaUtils morphiaUtils = new MorphiaUtils();
			// Get list of all stocks
			List<StockVO> stockVOList = morphiaUtils.getAllStocks();
			CompanyDetailsVO companyDetailsVO = morphiaUtils.getAllDetailsForCompany(symbol);
			
			if(stockVOList != null)
			{
				for(StockVO stockVO : stockVOList)
				{
					if(stockVO.getSymbol().equalsIgnoreCase(symbol))
					{
						scriptAllDetailsVO = new ScriptAllDetailsVO();
						scriptAllDetailsVO.setName(stockVO.getName());
						scriptAllDetailsVO.setSymbol(stockVO.getSymbol());
						scriptAllDetailsVO.setTwitterSymbol(stockVO.getTwitterSymbol());
						
						if(companyDetailsVO != null)
						{
							scriptAllDetailsVO.setListingDate(companyDetailsVO.getListingDate());
							scriptAllDetailsVO.setFaceValue(AppUtils.parseDouble(companyDetailsVO.getFaceValue()));
							scriptAllDetailsVO.setIndustry(companyDetailsVO.getIndustry());
							scriptAllDetailsVO.setIsin(companyDetailsVO.getIsin());
							scriptAllDetailsVO.setIssuedCapInShares(companyDetailsVO.getIssuedCapInShares());
							scriptAllDetailsVO.setMarketCapInr(AppUtils.parseDouble(companyDetailsVO.getMarketCapInr()));
							scriptAllDetailsVO.setHigh52Week(AppUtils.parseDouble(companyDetailsVO.getHigh52Week()));
							scriptAllDetailsVO.setLow52Week(AppUtils.parseDouble(companyDetailsVO.getLow52Week()));
						}
						
						if(stockVO.getDailyDataVOList() != null && !stockVO.getDailyDataVOList().isEmpty())
						{
							for (StockDailyDataVO dailyDataVO : stockVO.getDailyDataVOList()) 
							{
								if(dailyDataVO.getDate().compareTo(AppUtils.getLastBusinessDate()) ==0)
								{
									scriptAllDetailsVO.setPresentableDate(AppUtils.getPresentableDate(dailyDataVO.getDate()));
									scriptAllDetailsVO.setOpen(AppUtils.parseDouble(dailyDataVO.getOpenPrice()));
									scriptAllDetailsVO.setClose(AppUtils.parseDouble((dailyDataVO.getClosePrice())));
									scriptAllDetailsVO.setHigh(AppUtils.parseDouble((dailyDataVO.getHighPrice())));
									scriptAllDetailsVO.setLow(AppUtils.parseDouble((dailyDataVO.getLowPrice())));
									scriptAllDetailsVO.setVolume(AppUtils.parseDouble((dailyDataVO.getTotalTradedQuantity())));
									scriptAllDetailsVO.setPreviousClose(AppUtils.parseDouble((dailyDataVO.getPreviousClose())));
									scriptAllDetailsVO.setChangeInPrice(AppUtils.parseDouble(scriptAllDetailsVO.getClose() - scriptAllDetailsVO.getPreviousClose()));
									if(scriptAllDetailsVO.getPreviousClose() > 0)
									{
										scriptAllDetailsVO.setPercentageChangeInPrice(AppUtils.parseDouble((100 * scriptAllDetailsVO.getChangeInPrice() / scriptAllDetailsVO.getPreviousClose())));
									}
									break;
								}
							}
						}
						break;
					}
				}
			}
		}
		catch(Exception e)
		{
			log.info("Exception in getAllStockDetails");
			e.printStackTrace();
		}

		log.info("Returning scriptAllDetailsVO");
		return scriptAllDetailsVO;
	}
}
