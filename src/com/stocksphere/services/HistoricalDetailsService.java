package com.stocksphere.services;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.stocksphere.common.AppUtils;
import com.stocksphere.services.SSAppServiceHelper;
import com.stocksphere.stockhistory.analysis.ScriptAllDetailsVO;
import com.stocksphere.stockhistory.analysis.ScriptHistoryAnalysisVO;

@Path("/get")
public class HistoricalDetailsService {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(HistoricalDetailsService.class);

	@GET
	@Path("/performingStocks")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStocksInJSON(@QueryParam("count") String count,
			@QueryParam("numberOfDays") String numberOfDays,
			@QueryParam("fromDate") String fromDate,
			@QueryParam("toDate") String toDate,
			@QueryParam("minRange") String minRange,
			@QueryParam("maxRange") String maxRange) {
		try
		{
			log.info("Inside getStocksInJSON");
			
			log.info("count: "+count);
			log.info("numberOfDays: "+numberOfDays);
			log.info("fromDate: "+fromDate);
			log.info("toDate: "+toDate);
			log.info("minRange: "+ minRange);
			log.info("maxRange: "+maxRange);
			
			//setting null to all undefined values
			numberOfDays = SSAppServiceUtils.setUndefinedNull(numberOfDays);
			fromDate = SSAppServiceUtils.setUndefinedNull(fromDate);
			toDate = SSAppServiceUtils.setUndefinedNull(toDate);
			minRange = SSAppServiceUtils.setUndefinedNull(minRange);
			maxRange = SSAppServiceUtils.setUndefinedNull(maxRange);
			
			
			SSAppServiceHelper analyser = new SSAppServiceHelper();

			List<ScriptHistoryAnalysisVO> analysisVOList = analyser.getStockList(numberOfDays, fromDate, toDate, minRange, maxRange);
			Map<String,String> map = null;
			JSONObject jsonObject = null;
			JSONArray jsonArray= null;
			
			int counter=1;
			int countParam = Integer.parseInt(count);

			if(analysisVOList != null && !analysisVOList.isEmpty())
			{
				jsonArray= new JSONArray();
				
				for(ScriptHistoryAnalysisVO analysisVO : analysisVOList)
				{
					map = new HashMap<String,String>();
					map.put( "symbol", analysisVO.getSymbol());
					map.put( "name", analysisVO.getName());
					map.put( "percentage",Double.toString(AppUtils.parseDouble(analysisVO.getPercentageDiffClosingPrice())));
					map.put( "closingPriceDiff",Double.toString(AppUtils.parseDouble(analysisVO.getDiffClosingPrice())));
					map.put( "startClosingPrice", Double.toString(analysisVO.getStartClosingPrice()));
					map.put( "endClosingPrice", Double.toString(analysisVO.getEndClosingPrice()));
					map.put( "averageVolume", Double.toString(analysisVO.getAverageVolume()));
					map.put( "averageClosingPrice", Double.toString(analysisVO.getAverageClosingPrice()));
					map.put( "lowPrice", Double.toString(analysisVO.getLowPrice()));
					map.put( "highPrice", Double.toString(analysisVO.getHighPrice()));

					SimpleDateFormat sdf = new SimpleDateFormat(SSAppServiceConstants.DATE_FOTMAT);
					map.put( "startDate", sdf.format(analysisVO.getStartDate()));
					map.put( "endDate", sdf.format(analysisVO.getEndDate()));
					map.put( "twitterSymbol", analysisVO.getTwitterSymbol());
					jsonObject = new JSONObject(map);
					jsonArray.put(jsonObject);
					
					if(countParam <= analysisVOList.size() && (counter >= countParam))
					{
						break;
					}
					else
					{
						counter++;
					}
				}
				JSONObject finalObject = new JSONObject();
		        finalObject.put("stock_data", jsonArray);
				
		        return Response.status(200).entity(finalObject.toString()).build();	
			}
			else
			{
				log.info("analysisVOList is null or empty");
			}
		}catch(Exception e)
		{
			log.error("Exception occurred in getStocksInJSON. "+e.getMessage());
		}
		 return Response.status(201).entity("Exception in getStocksInJSON").build();	
	}
	
	@GET
	@Path("/detailsForScript")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDailyDetailsOfScriptInJSON(@QueryParam("symbol") String symbol) {

		try
		{
			log.info("Inside getDailyDetailsOfScriptInJSON");
			log.info("Symbol: "+symbol);
			
			SSAppServiceHelper analyser = new SSAppServiceHelper();

			List<ScriptAllDetailsVO> scriptAllDetailsVOList = analyser.getAllDailyDetailsForScript(symbol);
			Map<String,String> map = null;
			JSONObject jsonObject = null;
			JSONArray jsonArray= null;
			
			if(scriptAllDetailsVOList != null && !scriptAllDetailsVOList.isEmpty())
			{
				jsonArray= new JSONArray();
				
				for(ScriptAllDetailsVO scriptAllDetailsVO : scriptAllDetailsVOList)
				{
					map = new HashMap<String,String>();
					map.put( "symbol", scriptAllDetailsVO.getSymbol());
					map.put( "name", scriptAllDetailsVO.getName());
					map.put( "open",Double.toString(AppUtils.parseDouble(scriptAllDetailsVO.getOpen())));
					map.put( "high",Double.toString(AppUtils.parseDouble(scriptAllDetailsVO.getHigh())));
					map.put( "low",Double.toString(AppUtils.parseDouble(scriptAllDetailsVO.getLow())));
					map.put( "close",Double.toString(AppUtils.parseDouble(scriptAllDetailsVO.getClose())));
					map.put( "volume",Double.toString(AppUtils.parseDouble(scriptAllDetailsVO.getVolume())));
					map.put( "date",Long.toString(scriptAllDetailsVO.getDate().getTime()));// date in milis
					jsonObject = new JSONObject(map);
					jsonArray.put(jsonObject);
				}
				JSONObject finalObject = new JSONObject();
		        finalObject.put("stock_data", jsonArray);
				
		        return Response.status(200).entity(finalObject.toString()).build();	
			}
			else
			{
				log.info("scriptAllDetailsVOList is null or empty");
			}
		}catch(Exception e)
		{
			log.error("Exception occurred in getDailyDetailsOfScriptInJSON. "+e.getMessage());
		}
		 return Response.status(201).entity("Exception in getDailyDetailsOfScriptInJSON").build();	
	}
	
	@GET
	@Path("/allScriptNames")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllScriptsNameInJSON() {

		try
		{
			SSAppServiceHelper analyser = new SSAppServiceHelper();

			List<ScriptHistoryAnalysisVO> allScriptNameList = analyser.getAllScriptNames();
			Map<String,String> map = null;
			JSONObject jsonObject = null;
			JSONArray jsonArray= null;
			

			if(allScriptNameList != null && !allScriptNameList.isEmpty())
			{
				jsonArray= new JSONArray();
				
				for(ScriptHistoryAnalysisVO scriptHistoryAnalysisVO : allScriptNameList)
				{
					map = new HashMap<String,String>();
					map.put( "symbol", scriptHistoryAnalysisVO.getSymbol());
					map.put( "name", scriptHistoryAnalysisVO.getName());
					jsonObject = new JSONObject(map);
					jsonArray.put(jsonObject);
				}
				JSONObject finalObject = new JSONObject();
		        finalObject.put("stock_data", jsonArray);
				
		        return Response.status(200).entity(finalObject.toString()).build();	
			}
			else
			{
				log.info("allScriptNameList is null or empty");
			}
		}catch(Exception e)
		{
			log.error("Exception occurred in getAllScriptsNameInJSON. "+e.getMessage());
		}
		 return Response.status(201).entity("Exception in getAllScriptsNameInJSON").build();	
	}
	
	@GET
	@Path("/allDetailsForScript")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllDetailsOfScriptInJSON(@QueryParam("symbol") String symbol) {

		try
		{
			log.info("Inside getAllDetailsOfScriptInJSON");
			log.info("Symbol: "+symbol);

			SSAppServiceHelper analyser = new SSAppServiceHelper();

			ScriptAllDetailsVO scriptAllDetailsVO = analyser.getAllDetailsOfStock(symbol);
			Map<String,String> map = null;
			JSONObject jsonObject = null;
			JSONArray jsonArray= null;

			if(scriptAllDetailsVO != null)
			{
				jsonArray= new JSONArray();

				map = new HashMap<String,String>();
				map.put( "symbol", scriptAllDetailsVO.getSymbol());
				map.put( "name", scriptAllDetailsVO.getName());
				map.put( "open", Double.toString(scriptAllDetailsVO.getOpen()));
				map.put( "close", Double.toString(scriptAllDetailsVO.getClose()));
				map.put( "previousClose", Double.toString(scriptAllDetailsVO.getPreviousClose()));
				map.put( "high", Double.toString(scriptAllDetailsVO.getHigh()));
				map.put( "low", Double.toString(scriptAllDetailsVO.getLow()));
				map.put( "changeInPrice", Double.toString(scriptAllDetailsVO.getChangeInPrice()));
				map.put( "percentageChangeInPrice", Double.toString(scriptAllDetailsVO.getPercentageChangeInPrice()));
				map.put( "date", scriptAllDetailsVO.getPresentableDate());
				map.put( "volume", Double.toString(scriptAllDetailsVO.getVolume()));
				
				map.put( "listingDate", scriptAllDetailsVO.getListingDate());
				map.put( "faceValue", Double.toString(scriptAllDetailsVO.getFaceValue()));
				map.put( "industry", scriptAllDetailsVO.getIndustry());
				map.put( "isin", scriptAllDetailsVO.getIsin());
				map.put( "issuedCapInShares", Long.toString(scriptAllDetailsVO.getIssuedCapInShares()));
				map.put( "marketCapCrInr", Double.toString(scriptAllDetailsVO.getMarketCapInr()));
				map.put( "high52Week", Double.toString(scriptAllDetailsVO.getHigh52Week()));
				map.put( "low52Week", Double.toString(scriptAllDetailsVO.getLow52Week()));
				map.put( "twitterSymbol", scriptAllDetailsVO.getTwitterSymbol());
				
				jsonObject = new JSONObject(map);
				jsonArray.put(jsonObject);

				JSONObject finalObject = new JSONObject();
				finalObject.put("stock_data", jsonArray);

				return Response.status(200).entity(finalObject.toString()).build();	
			}
			else
			{
				log.info("scriptAllDetailsVOList is null or empty");
			}
		}catch(Exception e)
		{
			log.error("Exception occurred in getAllDetailsOfScriptInJSON. "+e.getMessage());
		}
		return Response.status(201).entity("Exception in getAllDetailsOfScriptInJSON").build();	
	}
}
