package com.stocksphere.stockhistory.analysis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.stocksphere.common.AppConstants;
import com.stocksphere.common.AppUtils;
import com.stocksphere.common.SSComparator;
import com.stocksphere.morphia.historydatavo.StockDailyDataVO;
import com.stocksphere.morphia.historydatavo.StockVO;

public class HistoryAnalyser {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(HistoryAnalyser.class);
	
	public List<ScriptHistoryAnalysisVO> analyse(List<StockVO> stockVOList, Date dateFrom, Date dateTo)
	{
		log.info("inside analyse method. Date range >> dateFrom: "+dateFrom +"  dateTo: "+dateTo);
		
		List<ScriptHistoryAnalysisVO> analysisVOList = null;
		ScriptHistoryAnalysisVO analysisVO = null;
		
		if(stockVOList != null && !stockVOList.isEmpty())
		{
			log.info("Size of stockVOList:  "+ stockVOList.size());
			
			analysisVOList = new ArrayList<ScriptHistoryAnalysisVO>();
			
			Date lastBusinessDate = AppUtils.getLastBusinessDate();
			
			for(StockVO stockVO : stockVOList)
			{
				analysisVO = new ScriptHistoryAnalysisVO();
				double volumeSum = 0.0;
				double closingPriceSum = 0.0;
				int daysCounter = 0;
				double lowPrice = 200000.0;
				double highPrice = 0.0;
				
				analysisVO.setSymbol(stockVO.getSymbol());
				analysisVO.setName(stockVO.getName());
				analysisVO.setStartDate(dateTo);// latest date
				analysisVO.setEndDate(dateFrom);
				analysisVO.setTwitterSymbol(stockVO.getTwitterSymbol());
				
				for(StockDailyDataVO dailyDataVO : stockVO.getDailyDataVOList())
				{
					if(dailyDataVO.getDate() != null)
					{
						if(dateTo.compareTo(lastBusinessDate) == 0 && dateTo.compareTo(dateFrom) == 0 && dailyDataVO.getDate().compareTo(dateTo) == 0)
						{
							analysisVO.setStartClosingPrice(dailyDataVO.getClosePrice());

							analysisVO.setEndClosingPrice(dailyDataVO.getPreviousClose());
						}
						else
						{
							if(dailyDataVO.getDate().compareTo(dateTo) == 0)
							{
								analysisVO.setStartClosingPrice(dailyDataVO.getClosePrice());
							}
							else if(dailyDataVO.getDate().compareTo(dateFrom) == 0)
							{
								analysisVO.setEndClosingPrice(dailyDataVO.getClosePrice());
							}
						}
						//if the dates are in range
						if (dailyDataVO.getDate().compareTo(dateFrom) >= 0
								&& (dailyDataVO.getDate().compareTo(dateTo) <= 0)) {

							volumeSum = volumeSum
									+ dailyDataVO.getTotalTradedQuantity();
							closingPriceSum = closingPriceSum
									+ dailyDataVO.getClosePrice();
							daysCounter++;

							if(dailyDataVO.getHighPrice() > highPrice)
							{
								highPrice = dailyDataVO.getHighPrice();
							}
							if(dailyDataVO.getLowPrice() < lowPrice)
							{
								lowPrice = dailyDataVO.getLowPrice();
							}
						}
					}
				}
				
				if(analysisVO.getStartClosingPrice() != 0 && analysisVO.getEndClosingPrice() != 0)
				{
					// get difference
					double diffClosingPrice = analysisVO.getStartClosingPrice()
							- analysisVO.getEndClosingPrice();
					analysisVO.setDiffClosingPrice(AppUtils.parseDouble(diffClosingPrice));
					// get percentage
					double hundred = 100;
					double percentage = AppUtils.parseDouble((diffClosingPrice * hundred)/analysisVO.getEndClosingPrice());
					analysisVO.setPercentageDiffClosingPrice(percentage);
					
					//get average volume and closing price
					double averageVolume = volumeSum/daysCounter;
					double averageClosingPrice = closingPriceSum/daysCounter;
					analysisVO.setAverageClosingPrice(AppUtils.parseDouble(averageClosingPrice));
					analysisVO.setAverageVolume(AppUtils.parseDouble(averageVolume));
					
					//set start and end date
					analysisVO.setStartDate(dateTo);
					analysisVO.setEndDate(dateFrom);
					
					//add low and high prices for range price
					analysisVO.setHighPrice(AppUtils.parseDouble(highPrice));
					analysisVO.setLowPrice(AppUtils.parseDouble(lowPrice));
					
					// add to list
					analysisVOList.add(analysisVO);
				}
				else
				{
					log.info(stockVO.getSymbol()+ " -> No data for the stock. Either date range is incorrect or stock price is zero on the given dates");
				}
			}
			
			SSComparator.COMPARISION_PARAMETER = AppConstants.COMPARISION_PARAMETER_PERCENTAGE;
			Collections.sort(analysisVOList,new SSComparator());
		}
		else
		{
			log.info("stockVOList is null or empty");
		}
		
		if(analysisVOList != null && !analysisVOList.isEmpty())
		{
			log.info("Exiting from analyse method. Returning analysisVOList with size:  "+analysisVOList.size());
		}
		else
		{
			log.info("Exiting from analyse method. Returning null analysisVOList ");
		}
		
		return analysisVOList;
	}
	
	public List<StockVO> filterStocks(List<StockVO> stockVOList, double minRange, double maxRange)
	{
		log.info("inside filterStocks with min range: "+ minRange +"  Max range: "+maxRange);
		
		StockVO stockVO = null;
		List<StockDailyDataVO> stockDailyDataVOList = null;
		List<StockVO> stockVOFiletredList = null;
		
		Date lastBusinessDate = AppUtils.getLastBusinessDate();
		
		if(stockVOList != null && !stockVOList.isEmpty())
		{
			log.info("size of stockVOList before filter : "+stockVOList.size());
			
			Iterator<StockVO> itr = stockVOList.iterator();
			
			stockVOFiletredList = new ArrayList<StockVO>();
			
			while(itr.hasNext())
			{
				stockVO = itr.next();
				boolean isPriceInRange = false;
				
				if(stockVO != null)
				{
					stockDailyDataVOList = stockVO.getDailyDataVOList();
					
					if(stockDailyDataVOList != null && !stockDailyDataVOList.isEmpty())
					{
						for(StockDailyDataVO stockDailyDataVO : stockDailyDataVOList)
						{
							if(stockDailyDataVO != null)
							{
								if(stockDailyDataVO.getDate().compareTo(lastBusinessDate) ==0 && stockDailyDataVO.getClosePrice() >= minRange && stockDailyDataVO.getClosePrice() <= maxRange)
								{
									isPriceInRange = true;
									break;
								}
							}
						}
					}
				}
				
				if(isPriceInRange)
				{
					// if price on last business date is in range then add it to filtered list
					stockVOFiletredList.add(stockVO);
				}
			}
			
			log.info("size of new stockVOList after filter : "+stockVOFiletredList.size());
		}
		else
		{
			log.info("stockVOList is null or empty");
		}
		
		log.info("Exiting filterStocks");
		return stockVOFiletredList;
	}
}
