package com.stocksphere.stockhistory.analysis;

import java.util.Date;

public class ScriptAllDetailsVO {
	
	private String symbol;
	
	private String name;
	
	private Date date;
	
	private double open;
	
	private double high;
	
	private double low;
	
	private double close;
	
	private double volume;
	
	private double previousClose;
	
	private double changeInPrice;
	
	private double percentageChangeInPrice;
	
	private String presentableDate;
	
	private String ListingDate;
	
	private double faceValue;
	
	private String isin;
	
	private String industry;
	
	private long issuedCapInShares;
	
	private double marketCapInr;
	
	private double high52Week;
	
	private double low52Week;
	
	private String twitterSymbol;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Date getDate() {
		return date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getPreviousClose() {
		return previousClose;
	}

	public void setPreviousClose(double previousClose) {
		this.previousClose = previousClose;
	}

	public double getChangeInPrice() {
		return changeInPrice;
	}

	public void setChangeInPrice(double changeInPrice) {
		this.changeInPrice = changeInPrice;
	}

	public double getPercentageChangeInPrice() {
		return percentageChangeInPrice;
	}

	public void setPercentageChangeInPrice(double percentageChangeInPrice) {
		this.percentageChangeInPrice = percentageChangeInPrice;
	}

	public String getPresentableDate() {
		return presentableDate;
	}

	public void setPresentableDate(String presentableDate) {
		this.presentableDate = presentableDate;
	}

	public String getListingDate() {
		return ListingDate;
	}

	public void setListingDate(String listingDate) {
		ListingDate = listingDate;
	}

	public double getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(double faceValue) {
		this.faceValue = faceValue;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public long getIssuedCapInShares() {
		return issuedCapInShares;
	}

	public void setIssuedCapInShares(long issuedCapInShares) {
		this.issuedCapInShares = issuedCapInShares;
	}

	public double getMarketCapInr() {
		return marketCapInr;
	}

	public void setMarketCapInr(double marketCapInr) {
		this.marketCapInr = marketCapInr;
	}

	public double getHigh52Week() {
		return high52Week;
	}

	public void setHigh52Week(double high52Week) {
		this.high52Week = high52Week;
	}

	public double getLow52Week() {
		return low52Week;
	}

	public void setLow52Week(double low52Week) {
		this.low52Week = low52Week;
	}

	public String getTwitterSymbol() {
		return twitterSymbol;
	}

	public void setTwitterSymbol(String twitterSymbol) {
		this.twitterSymbol = twitterSymbol;
	}
}
