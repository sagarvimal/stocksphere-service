package com.stocksphere.stockhistory.analysis;

import java.util.Date;

public class ScriptHistoryAnalysisVO {
	
	private String symbol;
	
	private String name;
	
	//to date
	private Date startDate;
	
	private double startClosingPrice;
	
	// from date
	private Date endDate;
	
	private double endClosingPrice;
	
	private double diffClosingPrice;
	
	private double averageVolume;
	
	private double averageClosingPrice;
	
	private double percentageDiffClosingPrice;
	
	private double lowPrice;
	
	private double highPrice;
	
	private String twitterSymbol;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public double getStartClosingPrice() {
		return startClosingPrice;
	}

	public void setStartClosingPrice(double startClosingPrice) {
		this.startClosingPrice = startClosingPrice;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public double getEndClosingPrice() {
		return endClosingPrice;
	}

	public void setEndClosingPrice(double endClosingPrice) {
		this.endClosingPrice = endClosingPrice;
	}

	public double getDiffClosingPrice() {
		return diffClosingPrice;
	}

	public void setDiffClosingPrice(double diffClosingPrice) {
		this.diffClosingPrice = diffClosingPrice;
	}

	public double getPercentageDiffClosingPrice() {
		return percentageDiffClosingPrice;
	}

	public void setPercentageDiffClosingPrice(double percentageDiffClosingPrice) {
		this.percentageDiffClosingPrice = percentageDiffClosingPrice;
	}

	public double getAverageVolume() {
		return averageVolume;
	}

	public void setAverageVolume(double averageVolume) {
		this.averageVolume = averageVolume;
	}

	public double getAverageClosingPrice() {
		return averageClosingPrice;
	}

	public void setAverageClosingPrice(double averageClosingPrice) {
		this.averageClosingPrice = averageClosingPrice;
	}

	public double getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(double minClosingPrice) {
		this.lowPrice = minClosingPrice;
	}

	public double getHighPrice() {
		return highPrice;
	}

	public void setHighPrice(double maxClosingPrice) {
		this.highPrice = maxClosingPrice;
	}

	public String getTwitterSymbol() {
		return twitterSymbol;
	}

	public void setTwitterSymbol(String twitterSymbol) {
		this.twitterSymbol = twitterSymbol;
	}
}
