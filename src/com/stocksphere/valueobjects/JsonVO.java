package com.stocksphere.valueobjects;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JsonVO {
	
	public JsonVO() {
	}
	
	public JsonVO(String symbol, double percentage) {
		
		this.symbol = symbol;
		this.percentage = percentage;
	}
	
	private String symbol;
	
	private double percentage;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
}
