package com.stocksphere.morphia;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.stocksphere.common.AppConstants;
import com.stocksphere.common.AppUtils;
import com.stocksphere.morphia.historydatavo.CompanyDetailsVO;
import com.stocksphere.morphia.historydatavo.StockDailyDataVO;
import com.stocksphere.morphia.historydatavo.StockVO;
import com.stocksphere.morphia.valueobjects.StockNewsVO;
import com.stocksphere.morphia.valueobjects.StockQuoteVO;

/*
 * https://github.com/mongodb/morphia/wiki/Query
 */
public class MorphiaUtils {

	private static org.apache.log4j.Logger log = Logger.getLogger(MorphiaUtils.class);

	private static MongoClient mongoClient = null;

	//Keeps the list of all stocks in db
	private static List<StockVO> stockVOList = null;

	public MorphiaUtils()
	{
		if(mongoClient == null)
		{
			try 
			{
				if(AppConstants.MONGO_DB_CONENCT_MODE_STR.equalsIgnoreCase(AppConstants.MONGO_DB_CONNECT_MODE))
				{
					MongoClientURI mongoClientURI = new MongoClientURI(AppConstants.MONGO_DB_CONNECTION_URI);
					mongoClient = new MongoClient(mongoClientURI);
				}
				else
				{
					mongoClient = new MongoClient(AppConstants.MONGO_DB_CONNECTION);
				}
			} catch (Exception e) 
			{
				log.error(e);
			}
		}
	}

	public void insertNewsData(String title, String subTitle, Date datePublished, String source, String url, String articleText,String portal)
	{
		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			// query to check if entry already exists
			Query<StockNewsVO> query = ds.createQuery(StockNewsVO.class).filter(AppConstants.NEWS_URL, url);

			StockNewsVO stockNewsVO = null;

			stockNewsVO = query.get();

			if(stockNewsVO != null && stockNewsVO.getUrl().equalsIgnoreCase(url))
			{
				log.info("Document alreday present for URL: "+ url);
			}
			else
			{
				stockNewsVO = new StockNewsVO();

				stockNewsVO.setTitle(title);
				stockNewsVO.setSubTitle(subTitle);
				stockNewsVO.setDatePublished(datePublished);
				stockNewsVO.setSource(source);
				stockNewsVO.setUrl(url);
				stockNewsVO.setArticleText(articleText);
				stockNewsVO.setPortal(portal);

				ds.save(stockNewsVO);

				log.info("Document inserted successfully for URL: "+ url);
			}
		}
		catch(Exception e)
		{
			log.error("Exception in inserting document for URL: "+ url);

			log.error(e);
		}
	}

	public void insertStockQuoteDetails(String scriptName, double lastMarketPrice, double changePrice, double changePercentage, double voulmeTraded, String portal)
	{
		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			// query to check if entry already exists
			Query<StockQuoteVO> query = ds.createQuery(StockQuoteVO.class).filter(AppConstants.SCRIPT_NAME, scriptName);

			StockQuoteVO stockQuoteVO = null;

			stockQuoteVO = query.get();

			if(stockQuoteVO != null && stockQuoteVO.getScriptName().equalsIgnoreCase(scriptName))
			{
				log.info("Document alreday present for script: "+ scriptName);
			}
			else
			{
				stockQuoteVO = new StockQuoteVO();

				stockQuoteVO.setScriptName(scriptName);
				stockQuoteVO.setLastMarketPrice(lastMarketPrice);
				stockQuoteVO.setChangePrice(changePrice);
				stockQuoteVO.setChangePercentage(changePercentage);
				stockQuoteVO.setVoulmeTraded(voulmeTraded);
				stockQuoteVO.setPortal(portal);

				ds.save(stockQuoteVO);

				log.info("Document inserted successfully for Script: "+ scriptName);
			}
		}
		catch(Exception e)
		{
			log.error("Exception in inserting document for Script: "+ scriptName);
			log.error(e);
		}
	}

	public void dropCollection(Class<?> collectionClass)
	{
		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			DBCollection dbCollection = ds.getCollection(collectionClass);

			if (dbCollection != null)
			{
				dbCollection.drop();
				log.info("Collection successfully removed : "+ dbCollection.getFullName());
			}
			else
			{
				log.info("Collection does not exists");
			}
		}
		catch(Exception e)
		{
			log.error(e);
		}
	}

	public DBCollection getCollection(String collectionName)
	{
		DBCollection dbCollection = null;

		try
		{
			DB db = mongoClient.getDB(AppConstants.DB_NAME);

			if (db.collectionExists(collectionName))
			{
				dbCollection = db.getCollection(collectionName);
			}
			else
			{
				log.info("Collection does not exists : "+ collectionName);
			}
		}
		catch(Exception e)
		{
			log.error(e);
		}

		return dbCollection;
	}

	public <T> List<T> getAllDocumentsOfCollection(Class<T> classsName,String condition)
	{
		log.info("Inside getAllDocumentsOfCollection ");
		
		Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

		Query<T> query = null;
		
		double startTime = System.currentTimeMillis();

		if(classsName.getName().equals(StockNewsVO.class.getName()))
		{
			if(condition != null && condition.equalsIgnoreCase(AppConstants.CONDITION_DATE_PUBLISHED))
			{
				query = ds.createQuery(classsName).field(AppConstants.DATE_PUBLISHED)
						.greaterThan(AppUtils.getDateLessThanStdHours());
			}
			else if(condition != null && condition.equalsIgnoreCase(AppConstants.CONDITION_WITHOUT_NEWS_SENTIMENT))
			{
				query = ds.createQuery(classsName).field(AppConstants.NEWS_SENTIMENT)
						.doesNotExist().field(AppConstants.DATE_PUBLISHED)
						.greaterThan(AppUtils.getDateLessThanStdHours());
			}
		}
		else if(classsName.getName().equals(StockQuoteVO.class.getName()) && condition != null &&  condition.equalsIgnoreCase(AppConstants.CONDITION_PERCENT_RANGE))
		{
			query = ds.createQuery(classsName).field(AppConstants.CHANGE_PERCENTAGE)
					.greaterThanOrEq(AppConstants.STOCK_PERCENT_RANGE);
		}
		else
		{
			query = ds.createQuery(classsName);
		}
		
		double endTime = System.currentTimeMillis();
		
		log.info("Total time of execution in getAllDocumentsOfCollection (milli seconds)=  " + (endTime-startTime));
		
		return (List<T>) query.asList();
	}

	public boolean isNewsAlreadyInserted(String url)
	{
		boolean isNewsInserted = false;

		Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

		Query<StockNewsVO> query = ds.createQuery(StockNewsVO.class).field(AppConstants.NEWS_URL).equal(url);

		List<StockNewsVO> docList = (List<StockNewsVO>) query.asList();

		if(docList != null && !docList.isEmpty())
		{
			isNewsInserted = true;
		}

		log.info("Doc inserted check: " +url+ "   "+isNewsInserted);

		return isNewsInserted;
	}

	public void updateNewsSentiment(StockNewsVO stockNewsVO)
	{
		log.info("Inside updateNewsSentiment");
		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);
			ds.save(stockNewsVO);

			log.info("Sentiment updated successfully");
		}
		catch(Exception e)
		{
			log.error("Exception in updateNewsSentiment");

			log.error(e);
		}
	}

	public void insertDailyStockDetails(StockVO stockVO)
	{
		log.info("Inside insertDailyStockDetails");
		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			Query<StockVO> query = ds.createQuery(StockVO.class).field("daily_details.date").equal(stockVO.getDailyDataVOList()
					.get(0).getDate()).field("symbol").equal(stockVO.getSymbol());

			StockVO doc = (StockVO) query.get();

			if(doc != null)
			{
				log.info("Document already inserted");
			}
			else
			{
				query = ds.createQuery(StockVO.class).field("symbol").equal(stockVO.getSymbol());

				doc = (StockVO) query.get();

				if(doc !=null)
				{
					List<StockDailyDataVO> dailyDataVOList = doc.getDailyDataVOList();
					dailyDataVOList.addAll(stockVO.getDailyDataVOList());
					doc.setDailyDataVOList(dailyDataVOList);
					ds.save(doc);
					log.info("DailyStockDetails inserted successfully");
				}
				else 
				{
					ds.save(stockVO);
					log.info("DailyStockDetails inserted successfully");
				}
			}	
		}
		catch(Exception e)
		{
			log.error("Exception in insertDailyStockDetails");

			log.error(e);
		}
	}

	public StockVO getAllDocumentsForScript(String scriptSymbol)
	{
		log.info("Inside getAllDocumentsForScript");

		StockVO stockVO = null;

		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			Query<StockVO> query = null;

			query = ds.createQuery(StockVO.class).field("symbol").equal(scriptSymbol);

			stockVO =  (StockVO) query.get();
		}catch(Exception e)
		{
			log.error("Exception in getAllDocumentsForScript");

			log.error(e);
		}

		return stockVO;
	}
	
	// this is currently not being used but will be used leter
	public List<StockVO> getAllStocks(double minRange, double maxRange, Date fromDate, Date toDate)
	{
		log.info("Inside getAllStocks((double minRange, double maxRange, Date fromDate, Date toDate))");

		List<StockVO> stockVOList = null;
		
		double startTime = System.currentTimeMillis();

		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			Query<StockVO> query = null;
			
			query = ds.createQuery(StockVO.class);
			
			/*if(minRange != 0 && maxRange != 0)
			{
				query = ds.createQuery(StockVO.class)
						.field("daily_details.close_price")
						.greaterThanOrEq(minRange)
						.field("daily_details.close_price")
						.lessThanOrEq(maxRange)
						.field("daily_details.date")
						.greaterThanOrEq(fromDate)
						.field("daily_details.date")
						.lessThanOrEq(toDate);
			}
			else
			{
				query = ds.createQuery(StockVO.class)
						.field("daily_details.date")
						.greaterThanOrEq(fromDate)
						.field("daily_details.date")
						.lessThanOrEq(toDate);
			}*/
			
			stockVOList =  (List<StockVO>) query.asList();

		}catch(Exception e)
		{
			log.error("Exception in getAllStocks((double minRange, double maxRange, Date fromDate, Date toDate))");

			log.error(e);
		}

		double endTime = System.currentTimeMillis();
		
		log.info("Total time of execution in getAllStocks (milli seconds)=  " + (endTime-startTime));
		
		return stockVOList;
	}
	
	public List<StockVO> getAllStocks()
	{
		log.info("Inside getAllStocks");

		if(stockVOList == null)
		{
			long startTime = System.currentTimeMillis();
			try
			{
				Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

				Query<StockVO> query = null;
				query = ds.createQuery(StockVO.class);

				//commented on July 23 2015 - implementing fetchEmptyEntities
				//stockVOList =  (List<StockVO>) query.asList();

				Iterable<StockVO> iterable = query.fetchEmptyEntities();

				if(iterable != null){
					log.info("query.fetchEmptyEntities() gives not null result");
					stockVOList = new ArrayList<StockVO>();
					Iterator<StockVO> itr = iterable.iterator();
					StockVO stockVO = null;

					while(itr.hasNext()){
						stockVO = itr.next();
						if(stockVO != null && stockVO.getId() != null){
							stockVO = getAllDocumentsForScript(stockVO.getId(), ds);
							stockVOList.add(stockVO);
						}
						else{
							log.info("stockVO is null in getAllStocks");
						}
					}

					long endTime = System.currentTimeMillis();
					log.info("Size of stockVOList is: "+stockVOList.size()+"  Total time of execution in getAllStocks (seconds)=  " + TimeUnit.MILLISECONDS.toSeconds((endTime-startTime)));
				}
				else{
					log.info("query.fetchEmptyEntities() is returning null");
				}

			}catch(Exception e)
			{
				log.error("Exception in getAllStocks");

				log.error(e);
			}
		}
		else
		{
			log.info("stockVOList is not null. it contains list of all stocks");
		}

		return stockVOList;
	}
	
	public StockVO getAllDocumentsForScript(ObjectId objectId, Datastore ds)
	{
		StockVO stockVO = null;

		try
		{
			Query<StockVO> query = null;
			query = ds.createQuery(StockVO.class).field("_id").equal(objectId);
			stockVO =  (StockVO) query.get();
		}catch(Exception e)
		{
			log.error("Exception in getAllDocumentsForScript. objectId: "+objectId);
			log.error(e);
		}
		return stockVO;
	}
	
	public void updateCompanyDetails(CompanyDetailsVO companyDetailsVO)
	{
		log.info("Inside updateCompanyDetails");
		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			Query<CompanyDetailsVO> query = ds.createQuery(CompanyDetailsVO.class).field("symbol").equal(companyDetailsVO.getSymbol());

			CompanyDetailsVO doc = (CompanyDetailsVO) query.get();

			if(doc != null)
			{
				log.info("Document already inserted for : "+companyDetailsVO.getSymbol());
				
				//if document already present, update the id 
				companyDetailsVO.setId(doc.getId());
			}
			
			ds.save(companyDetailsVO);
			log.info("CompanyDetails updated successfully for : "+companyDetailsVO.getSymbol());
		}
		catch(Exception e)
		{
			log.error("Exception in updateCompanyDetails for : "+companyDetailsVO.getSymbol());

			log.error(e);
		}
	}
	
	public CompanyDetailsVO getAllDetailsForCompany(String scriptSymbol)
	{
		log.info("Inside getAllDetailsForCompany");

		CompanyDetailsVO companyDetailsVO = null;

		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			Query<CompanyDetailsVO> query = null;

			query = ds.createQuery(CompanyDetailsVO.class).field("symbol").equal(scriptSymbol);

			companyDetailsVO =  (CompanyDetailsVO) query.get();
		}catch(Exception e)
		{
			log.error("Exception in getAllDetailsForCompany");

			log.error(e);
		}

		return companyDetailsVO;
	}
}
