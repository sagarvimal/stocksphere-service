package com.stocksphere.morphia.historydatavo;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

@Entity(value="db_col_his_details",noClassnameStored = true)
public class StockVO {
	
	@Id
	@Property("_id")
	private ObjectId id;
	
	@Property("symbol")
	private String symbol;
	
	@Property("name")
	private String name;
	
	@Property("twitter_symbol")
	private String twitterSymbol;
	
	@Embedded("daily_details")
	private List<StockDailyDataVO> dailyDataVOList;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<StockDailyDataVO> getDailyDataVOList() {
		return dailyDataVOList;
	}

	public void setDailyDataVOList(List<StockDailyDataVO> dailyDataVOList) {
		this.dailyDataVOList = dailyDataVOList;
	}

	public String getTwitterSymbol() {
		return twitterSymbol;
	}

	public void setTwitterSymbol(String twitterSymbol) {
		this.twitterSymbol = twitterSymbol;
	}
}
