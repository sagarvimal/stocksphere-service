package com.stocksphere.morphia.historydatavo;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import com.stocksphere.common.AppConstants;

@Entity(value="db_col_company_details",noClassnameStored = true)
public class CompanyDetailsVO {
	
	public CompanyDetailsVO() {

		this.updatedBy = AppConstants.BOT_NAME;
		
		this.updatedDateTime = new Date();
	}
	
	@Id
	@Property("_id")
	private ObjectId id;
	
	@Property("name")
	private String name;
	
	@Property("symbol")
	private String symbol;
	
	@Property("listing_date")
	private String ListingDate;
	
	@Property("face_value")
	private double faceValue;
	
	@Property("isin")
	private String isin;
	
	@Property("industry")
	private String industry;
	
	@Property("issued_cap_in_shares")
	private long issuedCapInShares;
	
	@Property("market_cap_in_cr_inr")
	private double marketCapInr;
	
	@Property("high_52_week")
	private double high52Week;
	
	@Property("low_52_week")
	private double low52Week;
	
	@Property("updated_date_time")
	private Date updatedDateTime;
	
	@Property("updated_by")
	private String updatedBy;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getListingDate() {
		return ListingDate;
	}

	public void setListingDate(String listingDate) {
		ListingDate = listingDate;
	}

	public double getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(double faceValue) {
		this.faceValue = faceValue;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public long getIssuedCapInShares() {
		return issuedCapInShares;
	}

	public void setIssuedCapInShares(long issuedCapInShares) {
		this.issuedCapInShares = issuedCapInShares;
	}

	public double getMarketCapInr() {
		return marketCapInr;
	}

	public void setMarketCapInr(double marketCapInr) {
		this.marketCapInr = marketCapInr;
	}

	public double getHigh52Week() {
		return high52Week;
	}

	public void setHigh52Week(double high52Week) {
		this.high52Week = high52Week;
	}

	public double getLow52Week() {
		return low52Week;
	}

	public void setLow52Week(double low52Week) {
		this.low52Week = low52Week;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}
}
