package com.stocksphere.morphia.historydatavo;

import java.util.Date;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

import com.stocksphere.common.AppConstants;

@Embedded
public class StockDailyDataVO {
	
	public StockDailyDataVO() {

		this.createdBy = AppConstants.BOT_NAME;
		
		this.createdDate = new Date();
	}
	
	@Property("series")
	private String series;
	
	@Property("date")
	private Date date;
	
	@Property("previous_close")
	private double previousClose;
	
	@Property("open_price")
	private double openPrice;
	
	@Property("high_price")
	private double highPrice;
	
	@Property("low_price")
	private double lowPrice;
	
	@Property("last_price")
	private double lastPrice;
	
	@Property("close_price")
	private double closePrice;
	
	@Property("average_price")
	private double averagePrice;
	
	@Property("total_traded_quantity")
	private double totalTradedQuantity;
	
	@Property("turnover_in_lacs")
	private double turnoverInLacs;
	
	@Property("deliverable_quantity")
	private double deliverableQuantity;
	
	@Property("paercentage_del_to_traded_qan")
	private double paercentageDelToTradedQan;
	
	@Property("created_date")
	private Date createdDate;
	
	@Property("created_by")
	private String createdBy;

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getPreviousClose() {
		return previousClose;
	}

	public void setPreviousClose(double previousClose) {
		this.previousClose = previousClose;
	}

	public double getOpenPrice() {
		return openPrice;
	}

	public void setOpenPrice(double openPrice) {
		this.openPrice = openPrice;
	}

	public double getHighPrice() {
		return highPrice;
	}

	public void setHighPrice(double highPrice) {
		this.highPrice = highPrice;
	}

	public double getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(double lowPrice) {
		this.lowPrice = lowPrice;
	}

	public double getLastPrice() {
		return lastPrice;
	}

	public void setLastPrice(double lastPrice) {
		this.lastPrice = lastPrice;
	}

	public double getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(double closePrice) {
		this.closePrice = closePrice;
	}

	public double getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(double averagePrice) {
		this.averagePrice = averagePrice;
	}

	public double getTotalTradedQuantity() {
		return totalTradedQuantity;
	}

	public void setTotalTradedQuantity(double totalTradedQuantity) {
		this.totalTradedQuantity = totalTradedQuantity;
	}

	public double getTurnoverInLacs() {
		return turnoverInLacs;
	}

	public void setTurnoverInLacs(double turnoverInLacs) {
		this.turnoverInLacs = turnoverInLacs;
	}

	public double getDeliverableQuantity() {
		return deliverableQuantity;
	}

	public void setDeliverableQuantity(double deliverableQuantity) {
		this.deliverableQuantity = deliverableQuantity;
	}

	public double getPaercentageDelToTradedQan() {
		return paercentageDelToTradedQan;
	}

	public void setPaercentageDelToTradedQan(double paercentageDelToTradedQan) {
		this.paercentageDelToTradedQan = paercentageDelToTradedQan;
	}
}
