package com.stocksphere.morphia.valueobjects;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import com.stocksphere.common.AppConstants;

@Entity(value="db_col_stock_quote_nse",noClassnameStored = true)
public class StockQuoteVO {
	
	public StockQuoteVO() {

		this.createdBy = AppConstants.BOT_NAME;
		
		this.createdDate = new Date();
	}
	
	@Id
	@Property("_id")
	private ObjectId id;
	
	@Property("script_name")
	private String scriptName; 
	
	@Property("last_market_price")
	private double lastMarketPrice;
	
	@Property("change_price")
	private double changePrice;
	
	@Property("change_percentage")
	private double changePercentage;
	
	@Property("volume_traded")
	private double voulmeTraded;
	
	@Property("portal")
	private String portal;
	
	@Property("created_date")
	private Date createdDate;
	
	@Property("created_by")
	private String createdBy;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getScriptName() {
		return scriptName;
	}

	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}

	public double getLastMarketPrice() {
		return lastMarketPrice;
	}

	public void setLastMarketPrice(double lastMarketPrice) {
		this.lastMarketPrice = lastMarketPrice;
	}

	public double getChangePrice() {
		return changePrice;
	}

	public void setChangePrice(double changePrice) {
		this.changePrice = changePrice;
	}

	public double getChangePercentage() {
		return changePercentage;
	}

	public void setChangePercentage(double changePercentage) {
		this.changePercentage = changePercentage;
	}

	public double getVoulmeTraded() {
		return voulmeTraded;
	}

	public void setVoulmeTraded(double voulmeTraded) {
		this.voulmeTraded = voulmeTraded;
	}

	public String getPortal() {
		return portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
