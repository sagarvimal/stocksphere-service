package com.stocksphere.common;

import java.util.Comparator;

import com.stocksphere.morphia.valueobjects.StockQuoteVO;
import com.stocksphere.stockhistory.analysis.ScriptAllDetailsVO;
import com.stocksphere.stockhistory.analysis.ScriptHistoryAnalysisVO;

public class SSComparator implements Comparator<Object>{
	
	//default comparison parameter 
	public static String COMPARISION_PARAMETER = AppConstants.COMPARISION_PARAMETER_PERCENTAGE;
	
	@Override
	public int compare(Object obj1, Object obj2) {
		
		if(obj1 instanceof StockQuoteVO && obj2 instanceof StockQuoteVO)
		{
			StockQuoteVO stockQuoteVO1 = (StockQuoteVO)obj1;
			StockQuoteVO stockQuoteVO2 = (StockQuoteVO)obj2;
			
			if(COMPARISION_PARAMETER.equals(AppConstants.COMPARISION_PARAMETER_PERCENTAGE))
			{
			    double delta= stockQuoteVO1.getChangePercentage() - stockQuoteVO2.getChangePercentage();
			    if(delta > 0) return -1;
			    if(delta < 0) return 1;
			}
		}
		
		/*if(obj1 instanceof StockNewsDataVO && obj2 instanceof StockNewsDataVO)
		{
			StockNewsDataVO stockNewsDataVO1 = (StockNewsDataVO)obj1;
			StockNewsDataVO stockNewsDataVO2 = (StockNewsDataVO)obj2;
			
			if(COMPARISION_PARAMETER.equals(AppConstants.COMPARISION_PARAMETER_DATE_PUBLISHED))
			{
			    return stockNewsDataVO2.getDatePublished().compareTo(stockNewsDataVO1.getDatePublished());
			}
		}*/
		
		if(obj1 instanceof ScriptHistoryAnalysisVO && obj2 instanceof ScriptHistoryAnalysisVO)
		{
			ScriptHistoryAnalysisVO analysisVO1 = (ScriptHistoryAnalysisVO)obj1;
			ScriptHistoryAnalysisVO analysisVO2 = (ScriptHistoryAnalysisVO)obj2;
			
			if(COMPARISION_PARAMETER.equals(AppConstants.COMPARISION_PARAMETER_PERCENTAGE))
			{
				double delta= analysisVO1.getPercentageDiffClosingPrice() - analysisVO2.getPercentageDiffClosingPrice();
			    if(delta > 0) return -1;
			    if(delta < 0) return 1;
			}
		}
		
		if(obj1 instanceof ScriptAllDetailsVO && obj2 instanceof ScriptAllDetailsVO)
		{
			ScriptAllDetailsVO scriptAllDetailsVO1 = (ScriptAllDetailsVO)obj1;
			ScriptAllDetailsVO scriptAllDetailsVO2 = (ScriptAllDetailsVO)obj2;
			
			if(COMPARISION_PARAMETER.equals(AppConstants.COMPARISION_PARAMETER_VOLUME))
			{
				double delta= scriptAllDetailsVO1.getDate().compareTo(scriptAllDetailsVO2.getDate());
			    if(delta > 0) return 1;
			    if(delta < 0) return -1;
			}
		}
		
		return 0;
	}
}
