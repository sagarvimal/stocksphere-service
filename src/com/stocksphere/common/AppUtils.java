package com.stocksphere.common;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Duration;

public class AppUtils {
	
	public static Date getDateLessThanStdHours()
	{
		return new Date(System.currentTimeMillis() - 60 * 60 * 1000 * AppConstants.STANDARD_HOURS) ;
	}
	
	public static boolean  isDateLessThanStdHours(Date date)
	{
		boolean isDateLessThanStdHours = false;
		
		if(date != null)
		{
			DateTime dateTimeNow = new DateTime(new Date());
			DateTime dateTime = new DateTime(date);
			Duration duration = new Duration(dateTime,dateTimeNow);
			
			if(duration != null && Math.abs(duration.getStandardHours()) <= AppConstants.STANDARD_HOURS)
			{
				isDateLessThanStdHours = true;
			}
		}
		return isDateLessThanStdHours;
	}
	
	public static Date getDateBeforeDays(int numOfdays)
	{
		// get all the holidays as java.util.Date
		List<Date> holidays = getAllNSEHolidays();

		// get the current date without the hours, minutes, seconds and millis
		Calendar cal = Calendar.getInstance();
		cal.setTime(getLastBusinessDate());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		// iterate over the dates from now and check if each days is a business day
		int businessDayCounter = 0;
		while (true)
		{
		    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		    if (dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY && !holidays.contains(cal.getTime())) 
		    {
		        businessDayCounter++;
		    }
		    
		    if(businessDayCounter >= numOfdays && dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY && !holidays.contains(cal.getTime()))
		    {
		    	break;
		    }
		    else
		    {
		    	cal.add(Calendar.DAY_OF_YEAR, -1);
		    }
		}
		return cal.getTime();
	}
	
	public static Date getLastBusinessDate()
	{
		// get all the holidays as java.util.Date
		List<Date> holidays = getAllNSEHolidays();

		// get the current date without the hours, minutes, seconds and millis
		Calendar cal = Calendar.getInstance();
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		
		// check for previous date if today's time is less than 10 PM
		if (dayOfWeek != Calendar.SATURDAY && dayOfWeek 
				!= Calendar.SUNDAY && !holidays.contains(cal.getTime()) && cal.get(Calendar.HOUR_OF_DAY) < AppConstants.ALLOWED_HOUR_OF_DAY) 
		{
			cal.add(Calendar.DAY_OF_YEAR, -1);
		}
		
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		while (true) 
		{
			dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY || holidays.contains(cal.getTime())) 
			{
				cal.add(Calendar.DAY_OF_YEAR, -1);
			}
			else
			{
				break;
			}
		}
		return cal.getTime();
	}
	
	private static List<Date> getAllNSEHolidays()
	{
		List<Date> holidayList = null;
		try
		{
			// muhurat trading date on Diwali is also included
			String holidays = "27-Feb-2014,17-Mar-2014,08-Apr-2014,14-Apr-2014,18-Apr-2014,24-Apr-2014,01-May-2014,"
					+ "29-Jul-2014,15-Aug-2014,29-Aug-2014,02-Oct-2014,03-Oct-2014,06-Oct-2014,15-Oct-2014,24-Oct-2014,"
					+ "04-Nov-2014,06-Nov-2014,25-Dec-2014,26-Jan-2015,17-Feb-2015,06-Mar-2015,02-Apr-2015,03-Apr-2015,"
					+ "14-Apr-2015,01-May-2015,17-Sep-2015,25-Sep-2015,02-Oct-2015,22-Oct-2015,12-Nov-2015,25-Nov-2015,"
					+ "25-Dec-2015";

			String [] holidayArray = holidays.split(AppConstants.COMMA);

			holidayList = new ArrayList<Date>();

			for(String dateStr : holidayArray)
			{
				holidayList.add(new SimpleDateFormat(AppConstants.NSE_DATE_FORMAT).parse(dateStr));
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception in getAllNationalHolidays");
			e.printStackTrace();
		}
		return holidayList;
	}
	
	public static double parseDouble(double value) {
		if (Double.isNaN(value)) {
			return 0.0;
		} else {
			return Double.parseDouble(new DecimalFormat("##.##").format(value));
		}
	}
	
	public static String getPresentableDate(Date date)
	{
		return new SimpleDateFormat(AppConstants.NSE_DATE_FORMAT).format(date);
	}
	
	public static Date getBusinessDate(Date date, String dateType)
	{
		// get all the holidays as java.util.Date
		List<Date> holidays = getAllNSEHolidays();

		// get the current date without the hours, minutes, seconds and millis
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		while (true) 
		{
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			
			if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY || holidays.contains(cal.getTime())) 
			{
				if(dateType.equalsIgnoreCase(AppConstants.DATE_TYPE_TO))
				{
					cal.add(Calendar.DAY_OF_YEAR, -1);
				}
				else if(dateType.equalsIgnoreCase(AppConstants.DATE_TYPE_FROM))
				{
					cal.add(Calendar.DAY_OF_YEAR, 1);
				}
			}
			else{
				break;
			}
		}
		return cal.getTime();
	}
}
